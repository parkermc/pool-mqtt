import argparse
import json
import random
import string


# Manages all the args and config data
class Config:

    # Flags
    VERBOSE = False
    MQTT_DEBUG = False
    GPIO = True

    # Json File
    MQTT = None

    RELAYS = []

    # Reads and sets all the variables from the json file
    @staticmethod
    def _READ_JSON(config_file):
        file_data = json.load(config_file)  # Load the file

        # If the MQTT fields are sent in json load them
        if "mqtt" in file_data:
            Config.MQTT = ConfigMQTT(file_data["mqtt"])

        # If there is a list of relays load it by looping trough the list
        if "relays" in file_data and isinstance(file_data["relays"], list):
            for relay in file_data["relays"]:
                Config.RELAYS.append(ConfigRelay(relay))

    # Inits/loads all the config options
    @staticmethod
    def INIT():
        # Phrase all the args/flags
        parser = argparse.ArgumentParser(description="" +
                                         "Control pool equipment via MQTT")
        parser.add_argument("-v", "--verbose",  action="store_true", dest="v",
                            help="Enables debug/verbose logging")
        parser.add_argument("--debug-mqtt",  action="store_true",
                            dest="debug-mqtt",
                            help="Enables debug post fix for mqtt")
        parser.add_argument("-c", "--config",
                            nargs=1, dest="c", default="config.json",
                            type=argparse.FileType('r'),
                            help="Sets the config files to use")

        args = parser.parse_args()

        # Set the right config options from the args/flags
        Config.VERBOSE = args.v
        Config.MQTT_DEBUG = args.v
        config_file = args.c

        # Load the config file
        if isinstance(config_file, list):
            config_file = config_file[0]
        Config._READ_JSON(config_file)
        config_file.close()

        # Check if the GPIO library is presant
        try:
            from RPi import GPIO
            Config.GPIO = True
            if Config.VERBOSE:
                print(GPIO.RPI_INFO)
        except ImportError as e:
            Config.GPIO = False
            if not Config.VERBOSE:
                raise(e)


# Holds the config for MQTT
class ConfigMQTT:
    def __init__(self, data):
        # Default values
        self._host = "localhost"
        self._port = 1883
        self._username = ""
        self._password = ""
        self._discovery_prefix = "homeassistant"

        # Set the values from json data if they are set
        if "host" in data:
            self._host = data["host"]
        if "port" in data:
            self._port = data["port"]
        if "username" in data:
            self._username = data["username"]
        if "password" in data:
            self._password = data["password"]
        if "discovery_prefix" in data:
            self._discovery_prefix = data["discovery_prefix"]

    # MQTT Host
    @property
    def host(self):
        return self._host

    # MQTT port
    @property
    def port(self):
        return self._port

    # MQTT username
    @property
    def username(self):
        return self._username

    # MQTT password
    @property
    def password(self):
        return self._password

    # Home assistant MQTT discovery prefix
    @property
    def discovery_prefix(self):
        return self._discovery_prefix


# Holds the config for each relay
class ConfigRelay:
    def __init__(self, data):
        # Set the id from the data or 8 letter random string
        if "id" in data:
            self._id = data["id"]
        else:
            self._id = ''.join(random.choice(string.ascii_lowercase)
                               for i in range(8))

        # Set the name from data or the id
        if "name" in data:
            self._name = data["name"]
        else:
            self._name = "ID " + self._id

        # Set the sensor device class from data or use the default
        if "device_class" in data:
            self._device_class = data["device_class"]
        else:
            self._device_class = "power"

        # Set the pin from data or error
        if "pin" in data:
            self._pin = data["pin"]
        else:
            raise RuntimeError("Relay pin not set. id=%s, name=%s"
                               % (self._id, self._name))
        print(self._id)

    # ID for the relay
    @property
    def id(self):
        return self._id

    # Name for the relay
    @property
    def name(self):
        return self._name

    # Device class for the binary senser for the relay
    @property
    def device_class(self):
        return self._device_class

    # The pin used to trigger the relay
    @property
    def pin(self):
        return self._pin


# Init config when module is loaded
Config.INIT()
