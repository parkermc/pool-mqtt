from config import Config
from debug import POOL_DEBUG
import paho.mqtt.client as mqtt

# The payloads for the availability topic
ONLINE = "online"
OFFLINE = "offline"


# Handles the connection to the MQTT server and all such data
class MQTT:

    # Ya know the thing
    def __init__(self, host, port, username, password, discovery_prefix):
        self._host = host
        self._port = port
        self._username = username
        self._password = password
        self._discovery_prefix = discovery_prefix
        self._connected = False
        self._interfaces = {}  # Keeps all the registered interfaces

        # Set the availability topic based on the prefix and name
        self._availability_topic = ("%s/device/%s/available"
                                    % (self._discovery_prefix, self._username))

        # Setup client
        self._client = mqtt.Client(client_id=self._username,
                                   clean_session=True, userdata=None,
                                   protocol=4)

        self._client.on_connect = self._on_connect
        self._client.on_disconnect = self._on_disconnect
        self._client.username_pw_set(self._username, password=self._password)

        # Set last will so availability will be set to offline (F in the chat)
        self._client.will_set(self._availability_topic, OFFLINE, retain=True)

        # Debug all the things!!!
        POOL_DEBUG(("Created MQTT Class with host=%s, port=%i, username=%s," +
                    "password=%s\n" +
                    "       discovery_prefix=%s, availability_topic=%s")
                   % (self._host, self._port, self._username,
                   "*"*len(self._password), self._discovery_prefix, self.
                      availability_topic))

    # The topic used to tell when the devices are online
    @property
    def availability_topic(self):
        return self._availability_topic

    # Gest the MQTT topic for the interface
    def get_interface_topic(self, interface):
        return ("%s/%s/%s"
                % (self._discovery_prefix, interface.device_type,
                   interface.id))

    # When connected update status and setup interfaces
    def _on_connect(self, client, userdata, flags, result_code):
        POOL_DEBUG("Connected to MQTT with result code: %s"
                   % mqtt.connack_string(result_code))

        # Update the status and set availability as online
        self._connected = True
        self._client.publish(self._availability_topic, ONLINE, retain=True)

        for i in self._interfaces.values():  # Tell the interfaces we're online
            i.on_connect()
            for j in i.topic_listeners:      # And register all their listeners
                self._client.subscribe(j)
                self._client.message_callback_add(j, self._on_message)
                POOL_DEBUG("Subscribed to %s" % j)

    # When disconnected change the status
    def _on_disconnect(self, client, userdata, result_code):
        POOL_DEBUG("Disconnected from MQTT")
        self._connected = False

    # When a message is received route it to the right interface
    def _on_message(self, client, userdata, msg):
        # Decode
        topic = msg.topic
        payload = msg.payload.decode('utf-8')

        POOL_DEBUG("Received message: topic=%s, msg=%s" % (topic, payload))

        for i in self._interfaces.values():   # Find the right interface
            if topic in i.topic_listeners:
                i.on_message(topic, payload)  # And give it the message

    # Registers the interface with MQTT
    def register_interface(self, interface):
        # Add it to the dict with it's new "id"
        key = "%s:%s" % (interface.device_type, interface.id)
        self._interfaces[key] = interface

        # Register all the sub interfaces
        for i in interface.sub_interfaces:
            self.register_interface(i)

        if self._connected:  # If online do all the on connect stuff
            self._interfaces[key].on_connect()   # Run callback
            for i in interface.topic_listeners:  # And register listeners
                self._client.subscribe(i)
                self._client.message_callback_add(i, self._on_message)
                POOL_DEBUG("Subscribed to %s" % i)

    # Connects to MQTT
    def connect(self):
        self._client.connect(self._host, self._port, 60)
        self._client.loop_start()

    # Disconnects from MQTT
    def disconnect(self):
        self._client.disconnect()

    # Publishes a message to the topic endpoint for the interface
    def publish(self, interface, endpoint, msg, retain=False):
        self._client.publish("%s/%s"
                             % (self.get_interface_topic(interface), endpoint),
                             msg, retain=retain)


# An class used to interface with MQTT
class MQTT_INTERFACE:

    # Ya know the thing
    def __init__(self, mqtt, id, name, device_type,
                 listen_to=[], sub_interfaces=[]):
        self._listen_topics = []
        self._device_type = device_type
        self._mqtt = mqtt
        self._sub_interfaces = sub_interfaces

        # Add dev postfix if needed
        self._id = id+("_dev" if Config.MQTT_DEBUG else "")
        self._name = name + (" Dev" if Config.MQTT_DEBUG else "")

        self._device_topic = self._mqtt.get_interface_topic(self)

        # Add all the topics to listen to
        for topic in listen_to:
            self._listen_topics.append("%s/%s" % (self._device_topic, topic))

    # The id of the interface
    @property
    def id(self):
        return self._id

    # The name of the interface
    @property
    def name(self):
        return self._name

    # The device type to use in the topic url
    @property
    def device_type(self):
        return self._device_type

    # All the sub interfaces that need to be registered
    @property
    def sub_interfaces(self):
        return self._sub_interfaces

    @property
    def topic_listeners(self):
        return self._listen_topics

    # Called when connected to MQTT
    def on_connect(self):
        pass

    # Called when a message is received in a topic the interface wants
    def on_message(self, topic, msg):
        pass
