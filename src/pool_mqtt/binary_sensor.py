import json

from debug import POOL_DEBUG
from mqtt import MQTT_INTERFACE

# The payloads for the sensor turning on or off
ON = "ON"
OFF = "OFF"


# Provides a link bettween MQTT and python for creating a BinarySensor
class BinarySensor(MQTT_INTERFACE):

    # Ya know the thing
    def __init__(self, mqtt, id, name, default_state, device_class):
        super().__init__(mqtt, id, name, "binary_sensor")
        self._device_class = device_class
        self._on = default_state

        POOL_DEBUG("Created binary sensor with id=%s" % self._id)

    # The config data to be sent over MQTT
    @property
    def _config_data(self):
        return {
            "name": self._name,
            "state_topic": "%s/state" % self._device_topic,
            "availability_topic": self._mqtt.availability_topic,
            "device_class": self._device_class
        }

    # Sends the current sensor state over MQTT
    def _send_state(self):
        self._mqtt.publish(self, "state", (ON if self._on else OFF),
                           retain=True)

    # Send the config and state when connected to MQTT
    def on_connect(self):
        self._mqtt.publish(self, "config", json.dumps(self._config_data),
                           retain=True)
        self._send_state()

    # Turn the binary sensor on and send state
    def on(self):
        self._on = True
        self._send_state()

    # Turn the binary sensor off and send state
    def off(self):
        self._on = False
        self._send_state()

    # Toggle the binary sensor between off and on then send state
    def toggle(self):
        self._on = not self._on
        self._send_state()
