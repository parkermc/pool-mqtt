from config import Config


# Prints the message only if the verbose argument is used
def POOL_DEBUG(*args):
    if Config.VERBOSE:
        print(*args)
