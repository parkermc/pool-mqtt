import json

from binary_sensor import BinarySensor
from config import Config
from debug import POOL_DEBUG
from mqtt import MQTT_INTERFACE

try:
    from RPi import GPIO
except ImportError:  # Config already tested to see if it would load
    pass

# The payloads for the current state
ON = "ON"
OFF = "OFF"


class Relay(MQTT_INTERFACE):

    # Ya know the thing
    def __init__(self, mqtt, id, name, pin, device_class):
        self._sensor = BinarySensor(mqtt, id, name, False, device_class)
        super().__init__(mqtt, id, name, "switch",
                         listen_to=["set"], sub_interfaces=[self._sensor])
        self._pin = pin
        self._on = False

        # Setup GPIO pins if the library is present
        if Config.GPIO:
            GPIO.setup(self._pin, GPIO.OUT)
            GPIO.output(self._pin, GPIO.HIGH)

        POOL_DEBUG("Created relay with id=%s, and GPIO pin=%s"
                   % (self._id, self._pin))

    # The config data to be sent over MQTT
    @property
    def _config_data(self):
        return {
            "name": self._name,
            "command_topic": "%s/set" % self._device_topic,
            "state_topic": "%s/state" % self._device_topic,
            "retain": True,
            "optimistic": True
        }

    # When connected to MQTT send the config and state
    def on_connect(self):
        self._mqtt.publish(self, "config", json.dumps(self._config_data),
                           retain=True)
        self._mqtt.publish(self, "state", (ON if self._on else OFF),
                           retain=True)

    # Called when a message gets sent to it's MQTT topic
    def on_message(self, topic, msg):
        if (not msg == ON) and (not msg == OFF):
            return     # If it is not the on or off payload stop here

        if msg == ON:  # If the on payload was received turn the relay on
            if Config.GPIO:
                GPIO.output(self._pin, GPIO.LOW)
            self._on = True
            self._sensor.on()
        else:         # Else the off payload was received so turn the relay off
            if Config.GPIO:
                GPIO.output(self._pin, GPIO.HIGH)
            self._on = False
            self._sensor.off()

        # Update relay state in mqtt
        POOL_DEBUG("Relay state changed: id=%s, on=%r" % (self._id, self._on))
        self._mqtt.publish(self, "state", (ON if self._on else OFF),
                           retain=True)
