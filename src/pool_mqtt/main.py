#!/usr/bin/python3

import signal

from config import Config
from debug import POOL_DEBUG
from mqtt import MQTT
from relay import Relay
from time import sleep

try:
    from RPi import GPIO
except ImportError:  # Config already tested to see if it would load
    pass

# Tells the while loop if the program has been killed
killed = False


# Called when the program has be told to die
def kill(a, b):
    global killed
    killed = True
    return


if __name__ == '__main__':
    # Setup GPIO
    if Config.GPIO:
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        POOL_DEBUG("GPIO mode set to BCM")
    else:
        POOL_DEBUG("GPIO mode not set, GPIO disabled")

    # Setup MQTT
    mqtt = MQTT(host=Config.MQTT.host,
                port=Config.MQTT.port,
                username=Config.MQTT.username,
                password=Config.MQTT.password,
                discovery_prefix=Config.MQTT.discovery_prefix
                )

    # Create and register the relays
    relays = []
    for relay_config in Config.RELAYS:
        relays.append(Relay(mqtt, relay_config.id, relay_config.name,
                            relay_config.pin, relay_config.device_class))
        mqtt.register_interface(relays[len(relays)-1])

    # TODO Create the temperature sensors

    # Connect to MQTT
    mqtt.connect()

    # Setup the kill listener
    signal.signal(signal.SIGINT, kill)
    signal.signal(signal.SIGTERM, kill)

    while not killed:
        # TODO update thermistor readings
        sleep(1)

    # Disconnect from MQTT peacefully
    mqtt.disconnect()

    # And print that is ended peacefully
    POOL_DEBUG("Program exited peacefully")
