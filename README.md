# Pool MQTT

A program that is used to control a pool over MQTT and home assistant.

The main.py file is setup where the pump relay is connected to pin 24, the cleaner/booster pump relay is connected to pin 23, and the relay for a valve actuator to control the flow to a solar heater on pin 27.