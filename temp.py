#!/usr/bin/env python
import time
import busio
import digitalio
import board
import math
import adafruit_mcp3xxx.mcp3008 as MCP
from adafruit_mcp3xxx.analog_in import AnalogIn

# create the spi bus
spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)

# create the cs (chip select)
cs = digitalio.DigitalInOut(board.D25)

# create the mcp object
mcp = MCP.MCP3008(spi, cs)

# create an analog input channel on pin 0
chan0 = AnalogIn(mcp, MCP.P0)

print('Raw ADC Value: ', chan0.value)
print('ADC Voltage: ' + str(chan0.voltage) + 'V')

last_read = 0       # this keeps track of the last potentiometer value
tolerance = 250     # to keep from being jittery we'll only change
# volume when the pot has moved a significant amount
# on a 16-bit ADC


def remap_range(value, left_min, left_max, right_min, right_max):
    # this remaps a value from original (left) range to new (right) range
    # Figure out how 'wide' each range is
    left_span = left_max - left_min
    right_span = right_max - right_min

    # Convert the left range into a 0-1 range (int)
    valueScaled = int(value - left_min) / int(left_span)

    # Convert the 0-1 range into a value in the right range.
    return int(right_min + (valueScaled * right_span))


while True:
    # we'll assume that the pot didn't move
    trim_pot_changed = False

    # read the analog pin
    trim_pot = chan0.value

    # how much has it changed since the last read?
    pot_adjust = abs(trim_pot - last_read)

    if pot_adjust > tolerance:
        trim_pot_changed = True

    if trim_pot_changed:
        volts = chan0.voltage
        if volts == 0:
            print("OPEN")
        else:
            # calculate the ohms using R1=(Vs*R2)/(Vo)-R2
            ohms = ((33000/volts))-10000

            lnohm = math.log1p(ohms)  # take ln(ohms)

            # a, b, & c values from http://www.thermistor.com/calculators.php
            # using curve R (-6.2%/C @ 25C) Mil Ratio X
            a = 0.002197222470870
            b = 0.000161097632222
            c = 0.000000125008328

            # Steinhart Hart Equation
            # T = 1/(a + b[ln(ohm)] + c[ln(ohm)]^3)

            t1 = (b*lnohm)  # b[ln(ohm)]

            c2 = c*lnohm  # c[ln(ohm)]

            t2 = math.pow(c2, 3)  # c[ln(ohm)]^3

            temp = 1/(a + t1 + t2)  # calcualte temperature

            tempc = temp - 273.15 - 4  # K to C
            # the -4 is error correction for bad python math
            tempf = tempc*9/5+32  # K to C

            # print out info
            print("V => %4.1f Ohms => %4.1f K => %4.1f C => %4.1f F => %4.1f"
                  % (volts, ohms, temp, tempc, tempf))
            # return tempc

            # save the potentiometer reading for the next loop
            last_read = trim_pot

    # hang out and do nothing for a half second
    time.sleep(0.5)
