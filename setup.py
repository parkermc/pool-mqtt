#!/usr/bin/python3

from setuptools import setup, find_packages

setup(
    name="Pool MQTT",
    version="0.0.1",
    install_requires=[
        "Adafruit-Blinka==5.1.0",
        "adafruit-circuitpython-busdevice==4.3.1",
        "adafruit-circuitpython-mcp3xxx==1.4.1",
        "Adafruit-PlatformDetect==2.14.0",
        "Adafruit-PureIO==1.1.5",
        "paho-mqtt==1.2",
        "pyftdi==0.51.2",
        "pyserial==3.4",
        "pyusb==1.0.2",
        "sysv-ipc==1.0.1",
        ],
    author="ParkerMc",
    author_email="parkermc@parkermc.net",
    description="A MQTT program to control a pool with home assistant",
    keywords="iot home-automation pool relay temperature heater thermistor",
    url="https://parkermc.net",
    packages=find_packages('src', include=["pool_mqtt", ]),
    entry_points={
        "console_scripts": [
            "pool_mqtt = pool_mqtt.main:main",
        ]
    }
)
